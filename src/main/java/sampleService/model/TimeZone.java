package sampleService.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;
import java.time.ZonedDateTime;

@Data
public class TimeZone {
    private String name;
    private int offset;
    @JsonProperty("current_time")
    private ZonedDateTime currentTime;
    @JsonProperty("current_time_unix")
    private Instant currentTimeUnix;
    @JsonProperty("is_dst")
    private boolean dst;
    @JsonProperty("dst_savings")
    private int dstSavings;
}
