package sampleService.controller;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import sampleService.model.Geolocation;
import sampleService.service.GeolocationService;

@Controller
public class LocationController {

    private final GeolocationService geolocationService;

    public LocationController(GeolocationService geolocationService) {
        this.geolocationService = geolocationService;
    }

    @ApiResponses({
            @ApiResponse(responseCode = "200")
    })
    @Get(uri = "/geolocation/{ip}", produces = MediaType.APPLICATION_JSON)
    public HttpResponse<Geolocation> GeoLocation(@PathVariable String ip) {
        Geolocation geolocation = geolocationService.getGeolocationByIp(ip);
        return HttpResponse.ok(geolocation);
    }
}
