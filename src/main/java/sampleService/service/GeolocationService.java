package sampleService.service;

import sampleService.model.Geolocation;
import sampleService.proxy.IpGeolocationProxy;

import javax.inject.Singleton;

@Singleton
public class GeolocationService {

    private final IpGeolocationProxy ipGeoProxy;

    public GeolocationService(IpGeolocationProxy ipGeoProxy) {
        this.ipGeoProxy = ipGeoProxy;
    }

    public Geolocation getGeolocationByIp(String ip) {
        Geolocation geolocation = ipGeoProxy.getGeolocationByIp(ip);
        return geolocation;
    }
}
