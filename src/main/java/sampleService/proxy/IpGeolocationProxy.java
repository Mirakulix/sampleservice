package sampleService.proxy;

import io.micronaut.context.annotation.Value;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import sampleService.model.Geolocation;

@Client
public interface IpGeolocationProxy {

    @Get(uri = "https://api.ipgeolocation.io/ipgeo?apiKey=8fdbef04caf34432a4c009f247869181&ip=217.80.173.34&lang=de")
    Geolocation getGeolocationByIp(String ip);
}
